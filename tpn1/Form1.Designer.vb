﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Increment_btn = New System.Windows.Forms.Button()
        Me.Decrement_btn = New System.Windows.Forms.Button()
        Me.RAZ_btn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Increment_btn
        '
        Me.Increment_btn.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!)
        Me.Increment_btn.Location = New System.Drawing.Point(566, 189)
        Me.Increment_btn.Name = "Increment_btn"
        Me.Increment_btn.Size = New System.Drawing.Size(139, 70)
        Me.Increment_btn.TabIndex = 0
        Me.Increment_btn.Text = "+"
        Me.Increment_btn.UseVisualStyleBackColor = True
        '
        'Decrement_btn
        '
        Me.Decrement_btn.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!)
        Me.Decrement_btn.Location = New System.Drawing.Point(91, 189)
        Me.Decrement_btn.Name = "Decrement_btn"
        Me.Decrement_btn.Size = New System.Drawing.Size(139, 70)
        Me.Decrement_btn.TabIndex = 1
        Me.Decrement_btn.Text = "-"
        Me.Decrement_btn.UseVisualStyleBackColor = True
        '
        'RAZ_btn
        '
        Me.RAZ_btn.Location = New System.Drawing.Point(346, 336)
        Me.RAZ_btn.Name = "RAZ_btn"
        Me.RAZ_btn.Size = New System.Drawing.Size(115, 45)
        Me.RAZ_btn.TabIndex = 2
        Me.RAZ_btn.Text = "RAZ"
        Me.RAZ_btn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(402, 227)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(13, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.RAZ_btn)
        Me.Controls.Add(Me.Decrement_btn)
        Me.Controls.Add(Me.Increment_btn)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Increment_btn As Button
    Friend WithEvents Decrement_btn As Button
    Friend WithEvents RAZ_btn As Button
    Friend WithEvents Label1 As Label
End Class
