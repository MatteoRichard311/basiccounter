﻿Imports ClassLibrary1


Public Class Form1


    Private Sub Decrement_btn_Click(sender As Object, e As EventArgs) Handles Decrement_btn.Click
        Class1.Decrement()
        Label1.Text = Class1.GetValue()
    End Sub

    Private Sub Increment_btn_Click(sender As Object, e As EventArgs) Handles Increment_btn.Click
        Class1.Increment()
        Label1.Text = Class1.GetValue()
    End Sub

    Private Sub RAZ_btn_Click(sender As Object, e As EventArgs) Handles RAZ_btn.Click
        Class1.Reset()
        Label1.Text = Class1.GetValue()
    End Sub
End Class
