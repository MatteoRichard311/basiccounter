﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Class1
    {
        public static int value = 0;


        public static void Increment()
        {
            value = value + 1;
        }

        public static void Decrement()
        {
            if(value >=1)
            value = value - 1;
        }

        public static void Reset()
        {
            value = 0;
        }

        public static int GetValue()
        {
            return value;
        {
}
    }

    }
}
